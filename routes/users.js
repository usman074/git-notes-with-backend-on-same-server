var express = require("express");
var router = express.Router();
const { loginUser } = require("../services/http-service");

router.get("/accessToken", async function (req, res) {
  const options = {
    method: "post",
    headers: { Accept: "application/json" },
  };

  const payload = {
    client_id: req.query.client_id,
    client_secret: req.query.client_secret,
    code: req.query.code,
  };
  for (item in payload) {
  }
  const url = `https://github.com/login/oauth/access_token`;
  const { data } = await loginUser(url, options, payload);
  res.send(data);
  res.end();
});

router.get("/login", async function (req, res) {
  const options = {
    method: "get",
    headers: { Authorization: "token " + req.query.accessToken },
  };

  const url = `https://api.github.com/user`;
  const { data } = await loginUser(url, options);
  res.send(data);
  res.end();
});

module.exports = router;
