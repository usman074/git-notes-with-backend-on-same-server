var express = require("express");
var router = express.Router();
const fs = require("fs");
const { loginUser, sendReq } = require("../services/http-service");

/* GET users listing. */
router.get("/public", async function (req, res) {
  try {
    const options = {
      method: "get",
      headers: { accept: "application/vnd.github.v3+json" },
    };
    const url = `gists/public?page=${req.query.currentPage}&per_page=10`;
    const { data } = await sendReq(url, options);
    const promises = data.map(async (item) => {
      const files = Object.keys(item.files);
      const contentPromises = files.map(async (file) => {
        const newOptions = {
          method: "get",
          headers: {
            accept: "application/vnd.github.v3+json",
            Authorization: req.headers.authorization,
          },
        };
        let { data: content } = await loginUser(
          item.files[file].raw_url,
          newOptions
        );

        if (typeof content !== String) {
          content = JSON.stringify(content);
        }
        item.files[file].content = content;
        return item;
      });
      const modifiedData = await Promise.all(contentPromises);
      return modifiedData[modifiedData.length - 1];
    });
    const responseData = await Promise.all(promises);
    res.send(responseData);
    res.end();
  } catch (e) {
    res.statusCode = 404;
    res.send("Not Found");
    // res.send("Something went wrong");
    res.end();
  }
});

router.get("/", async function (req, res) {
  try {
    const options = {
      method: "get",
      headers: {
        accept: "application/vnd.github.v3+json",
        Authorization: req.headers.authorization,
      },
    };
    const url = `gists?page=${req.query.currentPage}&per_page=10`;
    let { data } = await sendReq(url, options);

    const promises = data.map(async (item) => {
      const files = Object.keys(item.files);
      const contentPromises = files.map(async (file) => {
        const newOptions = {
          method: "get",
          headers: {
            accept: "application/vnd.github.v3+json",
            Authorization: req.headers.authorization,
          },
        };
        let { data: content } = await loginUser(
          item.files[file].raw_url,
          newOptions
        );
        item.files[file].content = content;
        return item;
      });
      const modifiedData = await Promise.all(contentPromises);
      return modifiedData[modifiedData.length - 1];
    });
    const responseData = await Promise.all(promises);
    res.send(responseData);
    res.end();
  } catch (e) {
    res.statusCode = 404;
    res.send("Not Found");
    res.end();
  }
});

router.get("/starred", async function (req, res) {
  try {
    const options = {
      method: "get",
      headers: {
        accept: "application/vnd.github.v3+json",
        Authorization: req.headers.authorization,
      },
    };
    const url = `gists/starred?page=${req.query.currentPage}&per_page=10`;
    const { data } = await sendReq(url, options);
    const promises = data.map(async (item) => {
      const files = Object.keys(item.files);
      const contentPromises = files.map(async (file) => {
        const newOptions = {
          method: "get",
          headers: {
            accept: "application/vnd.github.v3+json",
            Authorization: req.headers.authorization,
          },
        };
        let { data: content } = await loginUser(
          item.files[file].raw_url,
          newOptions
        );
        item.files[file].content = content;
        return item;
      });
      const modifiedData = await Promise.all(contentPromises);
      return modifiedData[modifiedData.length - 1];
    });
    const responseData = await Promise.all(promises);
    res.send(responseData);
    res.end();
  } catch (e) {
    res.statusCode = 404;
    res.send("Not Found");
    res.end();
  }
});

router.get("/:id", async function (req, res) {
  try {
    const options = {
      method: "get",
      headers: {
        accept: "application/vnd.github.v3+json",
        Authorization: req.headers.authorization,
      },
    };
    const url = `gists/${req.params.id}`;
    const { data } = await sendReq(url, options);
    if (data) {
      res.send(data);
      res.end();
    } else {
      throw new Error("Not Found");
    }
  } catch (e) {
    res.statusCode = 404;
    res.send("Not Found");
    // res.send("Something went wrong");
    res.end();
  }
});

router.put("/", async function (req, res) {
  try {
    const options = {
      method: "patch",
      headers: {
        accept: "application/vnd.github.v3+json",
        Authorization: req.headers.authorization,
      },
    };
    const url = `gists/${req.body.gist.id}`;
    const { data } = await sendReq(url, options, req.body.gist);
    res.send({ data });
    res.end();
  } catch (e) {
    res.statusCode = 404;
    res.send("Not Found");
    res.end();
  }
});

router.post("/", async function (req, res) {
  try {
    const options = {
      method: "post",
      headers: {
        accept: "application/vnd.github.v3+json",
        Authorization: req.headers.authorization,
      },
    };
    const url = `gists`;
    const { data } = await sendReq(url, options, req.body);
    res.send(data);
    res.end();
  } catch (e) {
    res.statusCode = 404;
    res.send("Not Found");
    res.end();
  }
});

router.post("/fork", async function (req, res) {
  try {
    const options = {
      method: "post",
      headers: {
        accept: "application/vnd.github.v3+json",
        Authorization: req.headers.authorization,
      },
    };
    const url = `gists/${req.body.id}/forks`;
    const { data } = await sendReq(url, options);
    res.send(data);
    res.end();
  } catch (e) {
    res.statusCode = 404;
    res.send("Not Found");
    res.end();
  }
});

router.delete("/:id", async function (req, res) {
  try {
    const options = {
      method: "delete",
      headers: {
        accept: "application/vnd.github.v3+json",
        Authorization: req.headers.authorization,
      },
    };
    const url = `gists/${req.params.id}`;
    const response = await sendReq(url, options);
    if (response.status === 204) {
      res.send("Gist deleted");
      res.end();
    } else {
      throw new Error("Something went wrong");
    }
  } catch (e) {
    res.statusCode = 404;
    res.send("Not Found");
    res.end();
  }
});

module.exports = router;
