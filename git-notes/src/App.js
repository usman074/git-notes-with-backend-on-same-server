import React, { useState } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import "./App.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { UserProvider } from "./services/userCotextService";
import Header from "./components/Header";
import PublicGistTable from "./components/PublicGistTable";
import UserGistTable from "./components/UsersGistTable";
import StarredGistTable from "./components/StarredGistTable";
import Login from "./components/Login";
import GistCode from "./components/GistCode";
import { access_token_key } from "./authConfig.json";
import { ToastContainer } from "react-toastify";
import GistCodeEditor from "./components/GistCodeEditor";
import CreateGistCodeEditor from "./components/CreateGistCodeEditor";
import Profile from "./components/Profile";
import ProtectedRoute from "./components/common/ProtectedRoute";
import Signout from "./components/Signout/signOut";
import SettingUserContext from "./components/SettingUserContext";
function App() {
  const [accessToken, setAccessToken] = useState(
    localStorage.getItem(access_token_key)
  );
  // const [searchQuery, setSearchQuery] = useState("");

  return (
    <UserProvider>
      <ToastContainer />
      <Header />
      <SettingUserContext accessToken={accessToken} />
      <Switch>
        <Route
          path="/login"
          render={(props) => (
            <Login {...props} setAccessToken={setAccessToken} />
          )}
        ></Route>
        <ProtectedRoute path="/signout" component={Signout}></ProtectedRoute>
        <ProtectedRoute
          path="/gists/starred"
          component={StarredGistTable}
        ></ProtectedRoute>
        <Route path="/gists/public" component={PublicGistTable}></Route>
        <ProtectedRoute
          path="/gists/create"
          component={CreateGistCodeEditor}
        ></ProtectedRoute>
        <ProtectedRoute
          path="/gists"
          component={UserGistTable}
        ></ProtectedRoute>
        <ProtectedRoute
          path="/gist/edit/:id"
          component={GistCodeEditor}
        ></ProtectedRoute>
        <ProtectedRoute path="/gist/:id" component={GistCode}></ProtectedRoute>
        <ProtectedRoute path="/profile" component={Profile}></ProtectedRoute>
        <Redirect from="/" to="/gists/public"></Redirect>
      </Switch>
    </UserProvider>
  );
}

export default App;
