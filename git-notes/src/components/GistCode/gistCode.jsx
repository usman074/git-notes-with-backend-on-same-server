import React, { useEffect, useState } from "react";
import { fetchSingleGist, delUserGist } from "../../actions/gistsAction";
import useApiCall from "../../customHooks/apiCallHook";
import Star from "../common/StarIcon";
import Fork from "../common/ForkIcon";
import Avtar from "../common/Avatar";
import Edit from "../common/EditIcon";
import Del from "../common/DelIcon";
import GistCodeBlock from "../common/GistCodeBlock";
import Loader from "react-loader-spinner";
import { toast } from "react-toastify";
import { useUserContext } from "../../services/userCotextService";

import {
  CodeBlockWrapper,
  CustomCodeBlock,
  MainWrapper,
  ActionWrapper,
  ProfileWrapper,
  MarginProfileDetails,
  RouteLink,
  LoaderWrapper,
  AlignLoader,
} from "./style";

const GistCode = (props) => {
  const { state: contextUser } = useUserContext();

  const [gists, setGists] = useState(null);
  const [gistState, callApi] = useApiCall({ response: null });
  const [gistDelState, gistDelApiCall] = useApiCall({ response: null });

  useEffect(() => {
    const gistId = props.match.params.id;
    const reqData = fetchSingleGist(gistId);

    async function fetchData() {
      const reqState = {
        response: null,
        error: "",
      };
      await callApi(reqData, reqState);
    }
    fetchData();
  }, []);

  useEffect(() => {
    if (gistState.error) {
      toast.error(gistState.error);
      props.history.push("/gists");
    } else if (gistState.response) {
      setGists(gistState.response);
    }
  }, [gistState]);

  useEffect(() => {
    if (gistDelState.error) {
      toast.error(gistDelState.error);
      props.history.push("/gists");
    } else if (gistDelState.response) {
      toast.success("Gist deleted");
      props.history.replace("/gists");
    }
  }, [gistDelState]);

  const handleDelGist = (id) => {
    const reqData = delUserGist(id);
    async function delGist() {
      const reqState = {
        response: null,
        error: "",
      };
      await gistDelApiCall(reqData, reqState);
    }
    delGist();
  };

  if (gistState.isLoading || !gists) {
    return (
      <LoaderWrapper>
        <AlignLoader>
          <Loader
            type="Oval"
            color="#0ca96e"
            height={100}
            width={100}
            visible={true}
          />
        </AlignLoader>
        <AlignLoader>
          <h3>Loading</h3>;
        </AlignLoader>
      </LoaderWrapper>
    );
  }

  if (!gistState.isLoading && gists) {
    const { id: gistId } = gists;
    const { avatar_url, login, id: userId } = gists.owner;
    return (
      <>
        <MainWrapper>
          <ProfileWrapper>
            <Avtar imgUrl={avatar_url} />
            <h4>{login}</h4>
          </ProfileWrapper>

          <ActionWrapper>
            {contextUser.user.id === userId && (
              <MarginProfileDetails>
                <RouteLink to={`/gist/edit/${gistId}`}>
                  <Edit /> <span>Edit</span>
                </RouteLink>
              </MarginProfileDetails>
            )}
            {contextUser.user.id === userId && (
              <MarginProfileDetails
                onClick={() => {
                  handleDelGist(gistId);
                }}
              >
                <Del /> <span>Delete</span>
              </MarginProfileDetails>
            )}
            <MarginProfileDetails>
              <Star /> <span>Star</span>
            </MarginProfileDetails>

            {contextUser.user.id !== userId && (
              <MarginProfileDetails>
                <Fork gistId={gistId} /> <span>Fork</span>
              </MarginProfileDetails>
            )}
          </ActionWrapper>
        </MainWrapper>
        <CodeBlockWrapper>
          <CustomCodeBlock>
            <GistCodeBlock gists={gists} />
          </CustomCodeBlock>
        </CodeBlockWrapper>
      </>
    );
  }
};

export default GistCode;
