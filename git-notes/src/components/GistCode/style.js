import styled from "styled-components";
import { Link } from "react-router-dom";

export const CodeBlockWrapper = styled.div`
  display: grid;
  grid-template-columns: 15% 70% 15%;
  grid-template-rows: 1fr;
`;

export const CustomCodeBlock = styled.div`
  grid-column: 2/3;
`;

export const MainWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 2fr 2fr 2fr 1fr;
  grid-template-rows: 1fr;
`;

export const ProfileWrapper = styled.div`
  grid-column: 2/3;
  display: flex;
  flex-direction: row;
  align-items: center;
  color: #4a81d4;
`;

export const ActionWrapper = styled.div`
  grid-column: 4/5;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;

export const MarginProfileDetails = styled.div`
  margin-left: 5%;
  color: #4a81d4;
  cursor: pointer;
`;

export const LoaderWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
  // margin-left: 20px;
`;

export const AlignLoader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 20px;
`;

export const RouteLink = styled(Link)`
  color: #4a81d4;
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
`;
