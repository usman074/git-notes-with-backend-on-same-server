import styled from "styled-components";

export const HeadingWrapper = styled.div`
  color: white;
  font-size: 20px;
  background-color: #5acba1;
  padding: 20px 0px 20px 15px;
  display: grid;
  grid-template-columns: 1fr 4fr 1fr;
  grid-template-rows: 1fr;
`;

export const SearchBarWrapper = styled.div`
  width: 100%;
  grid-column: 2/3;
`;

export const ImageWrapper = styled.div`
  width: 100%;
  grid-column: 1/2;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const CustomImage = styled.img`
  filter: brightness(8);
  width: 100%;
`;

export const ButtonWrapper = styled.div`
  grid-column: 3/4;
  width: 100%;
`;
