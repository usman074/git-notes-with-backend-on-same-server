import React from "react";
import logo from "../../assets/images/logo.png";
import SearchBar from "../common/SearchBar";
import Button from "../common/Button";
import ProfileMenu from "../ProfileMenu";
import { useUserContext } from "../../services/userCotextService";

import { AuthorizationUrl, client_id, scope } from "../../authConfig.json";
import {
  HeadingWrapper,
  CustomImage,
  ImageWrapper,
  ButtonWrapper,
  SearchBarWrapper,
} from "./style";

const Header = () => {
  const buttonProps = {
    text: "Login",
    backgroundColor: "white",
    color: "#0ca96e",
    borderRadius: "5px",
    width: "60%",
  };
  const { state: contextUser } = useUserContext();

  // const handleSearchQueryChange = ({ currentTarget: input }) => {
  //   const { value } = input;
  //   setSearchQuery(value);
  // };

  const handleLoginClick = async () => {
    window.location = `${AuthorizationUrl}client_id=${client_id}&scope=${scope}`;
  };

  return (
    <HeadingWrapper>
      <ImageWrapper>
        <CustomImage src={logo} alt="logo" />
      </ImageWrapper>
      {/* <SearchBarWrapper>
        <SearchBar
          name="searchQuery"
          //value={searchQuery}
          //onChange={handleSearchQueryChange}
        />
      </SearchBarWrapper> */}
      <ButtonWrapper>
        {!contextUser.isLogin && (
          <Button btnProps={buttonProps} onClick={handleLoginClick} />
        )}
        {contextUser.isLogin && <ProfileMenu user={contextUser.user} />}
      </ButtonWrapper>
    </HeadingWrapper>
  );
};

export default Header;
