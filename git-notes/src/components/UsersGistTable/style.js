import styled from "styled-components";

export const LoaderWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
  // margin-left: 20px;
`;

export const AlignLoader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 20px;
`;

export const TableWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 12fr 1fr;
  grid-template-rows: 1fr;
`;

export const CustomTable = styled.div`
  grid-column: 2/3;
  padding-top: 3%;
  overflow-x: auto;
`;

export const SwitcherWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 12fr 1fr;
  grid-template-rows: 1fr;
`;

export const CodeBlockWrapper = styled.div`
  // display: grid;
  // grid-template-columns: 15% 70% 15%;
  // grid-template-rows: 1fr;
  margin: 0% 15%;
  width: 70%;
  display: grid;
  // margin-left: 0px;
  grid-column-gap: 3%;
  grid-template-columns: 33% 33% 33%;
`;

export const CustomSwitcher = styled.div`
  grid-column: 2/3;
  display: flex;
  justify-content: flex-end;
  padding-top: 3%;
`;

export const CustomGridIcon = styled.i`
  color: ${(props) => (props.selected ? "#676a68" : "#4ae665")};
  margin-left: 2px;
`;

export const CustomListIcon = styled.i`
  color: ${(props) => (props.selected ? "#4ae665" : "#676a68")};
  margin-left: 10px;
`;
