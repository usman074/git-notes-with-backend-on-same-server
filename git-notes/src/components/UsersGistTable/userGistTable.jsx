import React, { useState, useEffect } from "react";
import Table from "../common/Table";
import Pagination from "../common/Pagination";
import GistCodeBlock from "../common/GistCodeBlock";
import useApiCall from "../../customHooks/apiCallHook";
import { fetchUserGists } from "../../actions/gistsAction";
import Loader from "react-loader-spinner";
import {
  LoaderWrapper,
  TableWrapper,
  CustomTable,
  AlignLoader,
  CodeBlockWrapper,
  SwitcherWrapper,
  CustomSwitcher,
  CustomGridIcon,
  CustomListIcon,
} from "./style";
import { toast } from "react-toastify";

const UserGistTable = () => {
  const [userGistsState, callApi] = useApiCall({
    response: [],
  });
  const [currentPage, setCurrentPage] = useState(1);
  const [selected, setSelected] = useState(true);

  const [tableHeading] = useState([
    { path: "owner.login", width: "25%", label: "Name" },
    { path: "created_at", width: "14%", label: "Date" },
    { path: "created_at", width: "14%", label: "Time" },
    { path: "files", width: "14%", label: "Keyword" },
    { path: "files", width: "14%", label: "Notebook Name" },
    { path: "star", width: "5%", label: "" },
  ]);

  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    setTableData([]);
    const reqData = fetchUserGists(currentPage);
    async function fetchData() {
      const reqState = {
        response: [],
        error: "",
      };
      await callApi(reqData, reqState);
    }
    fetchData();
  }, [currentPage]);

  useEffect(() => {
    if (userGistsState.error) {
      toast.error(userGistsState.error);
      if (currentPage > 1) {
        setCurrentPage(currentPage - 1);
      }
    } else if (userGistsState.response) {
      setTableData(userGistsState.response);
    }
  }, [userGistsState]);

  const handlePageIncrement = () => {
    setCurrentPage(currentPage + 1);
  };

  const handlePageDecrement = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const handleGridView = () => {
    setSelected(false);
  };

  const handleListView = () => {
    setSelected(true);
  };

  if (userGistsState.isLoading && tableData.length >= 0) {
    return (
      <LoaderWrapper>
        <AlignLoader>
          <Loader
            type="Oval"
            color="#0ca96e"
            height={100}
            width={100}
            visible={true}
            //timeout={3000} //3 secs
          />
        </AlignLoader>
        <AlignLoader>
          <h3>Loading</h3>
        </AlignLoader>
      </LoaderWrapper>
    );
  }
  if (!userGistsState.isLoading && tableData.length > 0) {
    return (
      <>
        <SwitcherWrapper>
          <CustomSwitcher>
            <CustomGridIcon
              onClick={handleGridView}
              selected={selected}
              className="fa fa-th"
              aria-hidden="true"
            ></CustomGridIcon>
            <CustomListIcon
              onClick={handleListView}
              selected={selected}
              className="fa fa-list"
              aria-hidden="true"
            ></CustomListIcon>
          </CustomSwitcher>
        </SwitcherWrapper>

        {selected && (
          <TableWrapper>
            <CustomTable>
              <Table tableHeading={tableHeading} tableData={tableData} />
            </CustomTable>
          </TableWrapper>
        )}

        {!selected && (
          <CodeBlockWrapper>
            {tableData.map((gist) => (
              <GistCodeBlock gists={gist} width="80%" />
            ))}
          </CodeBlockWrapper>
        )}
        <Pagination
          onPageIncrement={handlePageIncrement}
          onPageDecrement={handlePageDecrement}
          currentPage={currentPage}
        />
      </>
    );
  }

  if (!userGistsState.isLoading && tableData.length === 0) {
    return (
      <>
        <h4>No Data Found</h4>
      </>
    );
  }
};

export default UserGistTable;
