import styled from "styled-components";
import { Link } from "react-router-dom";

export const Dropdown = styled.div`
  position: relative;
  display: inline-block;
`;

export const AvatarImage = styled.div`
  color: white;
  padding: 0px 16px 0px 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
`;

export const MenuOptionsWrapper = styled.div`
  display: block;
  display: ${(props) => (props.show ? "block" : "none")};
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  right: 25%;
  top: 53px;
  z-index: 1;
`;

export const MenuOptions = styled(Link)`
  color: #4e4d4de6;
  padding: 12px 16px;
  font-size: 16px;
  text-decoration: none;
  display: block;
  &:hover {
    background-color: #ddd;
  }
`;

export const Divider = styled.hr`
  margin: 0px 10px;
`;
