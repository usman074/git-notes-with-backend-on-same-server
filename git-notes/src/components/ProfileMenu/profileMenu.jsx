import React, { useState } from "react";
import Avatar from "../common/Avatar/index";
import {
  Dropdown,
  AvatarImage,
  MenuOptionsWrapper,
  MenuOptions,
  Divider,
} from "./style";

const ProfileMenu = ({ user }) => {
  const [showDropdown, setShowDropdown] = useState(false);

  const toggleDropdown = () => {
    setShowDropdown(!showDropdown);
  };
  return (
    <Dropdown>
      <AvatarImage>
        <Avatar onClick={toggleDropdown} imgUrl={user.avatar_url} />
      </AvatarImage>
      <MenuOptionsWrapper onClick={toggleDropdown} show={showDropdown}>
        <MenuOptions to="#">
          Signed in as <b>{user.login}</b>
        </MenuOptions>
        <Divider />
        <MenuOptions to="/gists">Your gists</MenuOptions>
        <MenuOptions to="/gists/public">Public gists</MenuOptions>
        <MenuOptions to="/gists/starred">Starred gists</MenuOptions>
        <MenuOptions to="/gists/create">Create gist</MenuOptions>
        <MenuOptions to="#">Help</MenuOptions>
        <Divider />
        <MenuOptions to="/profile">Your GitHub profile</MenuOptions>
        <MenuOptions to="/signout">Sign out</MenuOptions>
      </MenuOptionsWrapper>
    </Dropdown>
  );
};

export default ProfileMenu;
