import styled from "styled-components";

export const LoaderWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
  // margin-left: 20px;
`;

export const AlignLoader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 20px;
`;
