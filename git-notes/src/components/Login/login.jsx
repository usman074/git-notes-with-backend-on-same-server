import React, { useEffect } from "react";
import { getUserAccessToken } from "../../actions/authAction";
import useApiCall from "../../customHooks/apiCallHook";
import queryString from "query-string";
import { Redirect } from "react-router-dom";
import { toast } from "react-toastify";
import { LoaderWrapper, AlignLoader } from "./style";
import Loader from "react-loader-spinner";

import { access_token_key } from "../../authConfig.json";
import { useUserContext } from "../../services/userCotextService";

const Login = (props) => {
  const [accessToken, callApi] = useApiCall({ response: null });

  const { state: contextUser } = useUserContext();

  useEffect(() => {
    let params = queryString.parse(props.location.search);
    if ("code" in params) {
      const { code } = params;
      const reqData = getUserAccessToken(code);
      async function authenticateUser() {
        const reqState = {
          response: null,
          error: "",
        };
        await callApi(reqData, reqState);
      }
      authenticateUser();
      props.history.push("/login");
    }
  });

  useEffect(() => {
    if (accessToken.error) {
      toast.error(accessToken.error);
    } else if (accessToken.response) {
      localStorage.setItem(access_token_key, accessToken.response.access_token);
      props.setAccessToken(accessToken.response.access_token);
    }
  }, [accessToken]);

  if (contextUser.isLogin) {
    return <Redirect to="/gists/public" />;
  }
  if (accessToken.isLoading) {
    return (
      <LoaderWrapper>
        <AlignLoader>
          <Loader
            type="Oval"
            color="#0ca96e"
            height={100}
            width={100}
            visible={true}
            //timeout={3000} //3 secs
          />
        </AlignLoader>
        <AlignLoader>
          <h3>Loading</h3>;
        </AlignLoader>
      </LoaderWrapper>
    );
  }
  return null;
};

export default Login;
