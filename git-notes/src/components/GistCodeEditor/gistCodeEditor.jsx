import React, { useEffect, useState } from "react";
import { fetchSingleGist, updateUserGists } from "../../actions/gistsAction";
import useApiCall from "../../customHooks/apiCallHook";
import Avtar from "../common/Avatar";
import Button from "../common/Button";
import InputField from "../common/InputField";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import Editor from "react-simple-code-editor";
import { highlight, languages } from "prismjs/components/prism-core";
import "prismjs/components/prism-clike";
import "prismjs/components/prism-javascript";

import {
  CodeBlockWrapper,
  CustomCodeBlock,
  FileName,
  MainWrapper,
  ActionWrapper,
  ProfileWrapper,
  LoaderWrapper,
  AlignLoader,
  ButtonWrapper,
} from "./style";

const GistCodeEditor = (props) => {
  const [gists, setGists] = useState(null);
  const [gistFiles, setGistFiles] = useState([]);
  const [gistState, callApi] = useApiCall({ response: null });
  const [gistUpdateState, callGistUpdateApi] = useApiCall({ response: null });

  const [newGists, setNewGists] = useState([]);
  const [gistsFileName, setGistsFileName] = useState([]);

  const buttonProps = {
    text: "Update",
    backgroundColor: "#0ca96e",
    color: "white",
    height: "70%",
    borderRadius: "5px",
    width: "50%",
  };

  const delFileButtonProps = {
    text: "Delete File",
    backgroundColor: "#0ca96e",
    color: "white",
    height: "50%",
  };

  const addFileButtonProps = {
    text: "Add File",
    backgroundColor: "#0ca96e",
    color: "white",
    height: "70%",
    borderRadius: "5px",
    width: "50%",
  };

  useEffect(() => {
    const gistId = props.match.params.id;
    const reqData = fetchSingleGist(gistId);
    async function fetchData() {
      const reqState = {
        response: null,
        error: "",
      };
      await callApi(reqData, reqState);
    }
    fetchData();
  }, []);

  useEffect(() => {
    if (gistState.error) {
      toast.error(gistState.error);
    } else if (gistState.response) {
      setGists(gistState.response);
      const fileKeys = Object.keys(gistState.response.files);
      setGistFiles(fileKeys);
    }
  }, [gistState]);

  useEffect(() => {
    if (gistUpdateState.response) {
      toast.success("Gist Updated");
      props.history.push("/gists");
    }
  }, [gistUpdateState]);

  const handleValueChange = (gists, file, text) => {
    let updatedGist = { ...gists };
    updatedGist.files[file].content = text;
    setGists(updatedGist);
  };

  const handleGistUpdate = () => {
    const files = {};
    gistsFileName.map((fileName, index) => {
      files[fileName] = { content: newGists[index], filename: fileName };
    });
    let copyGists = gists;
    copyGists.files = { ...copyGists.files, ...files };
    const reqData = updateUserGists(copyGists);
    async function UpdateData() {
      const reqState = {
        response: {},
        error: "",
      };
      await callGistUpdateApi(reqData, reqState);
    }
    UpdateData();
  };

  const handleDelFile = (file, index) => {
    let copyGistFileName = [...gistFiles];
    const fileNameArr1 = copyGistFileName.splice(0, index);
    const fileNameArr2 = copyGistFileName.splice(1, index + 1);
    const delFileName = [...fileNameArr1, ...fileNameArr2];
    setGistFiles(delFileName);
    let copyGist = gists;
    delete copyGist.files[file];
    setGists(copyGist);
  };

  const handleAddFile = () => {
    const addFile = [...newGists, ""];
    const addFilename = [...gistsFileName, ""];
    setNewGists(addFile);
    setGistsFileName(addFilename);
  };

  const handleDelNewFile = (index) => {
    let copyGist = [...newGists];
    let copyGistFileName = [...gistsFileName];
    const gistArr1 = copyGist.splice(0, index);
    const gistArr2 = copyGist.splice(1, index + 1);
    const fileNameArr1 = copyGistFileName.splice(0, index);
    const fileNameArr2 = copyGistFileName.splice(1, index + 1);
    const delFile = [...gistArr1, ...gistArr2];
    const delFileName = [...fileNameArr1, ...fileNameArr2];
    setNewGists(delFile);
    setGistsFileName(delFileName);
  };

  const handleFileNameChange = ({ currentTarget: input }, index) => {
    const { value } = input;
    let fileNames = [...gistsFileName];
    fileNames[index] = value;
    setGistsFileName(fileNames);
  };

  const handleNewGistValueChange = (gists, index, text) => {
    let updatedGist = [...newGists];
    updatedGist[index] = text;
    setNewGists(updatedGist);
  };

  if (gistState.isLoading || !gists) {
    return (
      <LoaderWrapper>
        <AlignLoader>
          <Loader
            type="Oval"
            color="#0ca96e"
            height={100}
            width={100}
            visible={true}
          />
        </AlignLoader>
        <AlignLoader>
          <h3>Loading</h3>;
        </AlignLoader>
      </LoaderWrapper>
    );
  }

  if (!gistState.isLoading && gists) {
    const { avatar_url, login } = gists.owner;
    return (
      <>
        <MainWrapper>
          <ProfileWrapper>
            <Avtar imgUrl={avatar_url} />
            <h4>{login}</h4>
          </ProfileWrapper>
          <ActionWrapper>
            <Button btnProps={buttonProps} onClick={handleGistUpdate} />
          </ActionWrapper>
        </MainWrapper>
        {gistFiles.map((file, index) => (
          <CodeBlockWrapper key={gists.id + file}>
            <CustomCodeBlock>
              <FileName>
                <i className="fa fa-code" aria-hidden="true"></i>{" "}
                <span>{file}</span>
              </FileName>
              <Editor
                className="box"
                value={gists.files[file].content}
                onValueChange={(e) => {
                  handleValueChange(gists, file, e);
                }}
                highlight={(jsSample) => highlight(jsSample, languages.js)}
              />
            </CustomCodeBlock>
            <ButtonWrapper>
              <Button
                btnProps={delFileButtonProps}
                onClick={() => handleDelFile(file, index)}
              />
            </ButtonWrapper>
          </CodeBlockWrapper>
        ))}

        {newGists.map((newGist, index) => (
          <>
            <CodeBlockWrapper key={index + "code"}>
              <CustomCodeBlock key={index + "codeblock"}>
                <FileName>
                  <InputField
                    name="file name"
                    value={gistsFileName[index]}
                    onChange={(e) => handleFileNameChange(e, index)}
                  />
                </FileName>
                <Editor
                  className="box"
                  value={newGist}
                  onValueChange={(e) => {
                    handleNewGistValueChange(newGist, index, e);
                  }}
                  highlight={(jsSample) => highlight(jsSample, languages.js)}
                />
              </CustomCodeBlock>
              <ButtonWrapper key={index + "button"}>
                <Button
                  btnProps={delFileButtonProps}
                  onClick={() => handleDelNewFile(index)}
                />
              </ButtonWrapper>
            </CodeBlockWrapper>
          </>
        ))}
        <MainWrapper>
          <ActionWrapper>
            <Button btnProps={addFileButtonProps} onClick={handleAddFile} />
          </ActionWrapper>
        </MainWrapper>
      </>
    );
  }

  // return <h3>Loading</h3>;
};

export default GistCodeEditor;
