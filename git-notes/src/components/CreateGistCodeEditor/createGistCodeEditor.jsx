import React, { useEffect, useState } from "react";
import { createUserGists } from "../../actions/gistsAction";
import useApiCall from "../../customHooks/apiCallHook";
import Button from "../common/Button";
import InputField from "../common/InputField";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import Editor from "react-simple-code-editor";
import { highlight, languages } from "prismjs/components/prism-core";
import "prismjs/components/prism-clike";
import "prismjs/components/prism-javascript";

import {
  CodeBlockWrapper,
  CustomCodeBlock,
  FileName,
  MainWrapper,
  ActionWrapper,
  LoaderWrapper,
  AlignLoader,
} from "./style";

const CreateGistCodeEditor = (props) => {
  const [gists, setGists] = useState([""]);
  const [gistsFileName, setGistsFileName] = useState([""]);
  const [gistState, callGistCreateApi] = useApiCall({ response: null });

  const buttonProps = {
    text: "Create",
    backgroundColor: "#0ca96e",
    color: "white",
    height: "70%",
    borderRadius: "5px",
    width: "50%",
  };
  const addFileButtonProps = {
    text: "Add File",
    backgroundColor: "#0ca96e",
    color: "white",
    height: "70%",
    borderRadius: "5px",
    width: "50%",
  };
  const delFileButtonProps = {
    text: "Delete File",
    backgroundColor: "#0ca96e",
    color: "white",
    height: "70%",
  };

  useEffect(() => {
    if (gistState.response) {
      toast.success("Gist Created");
      props.history.push("/gists");
    }
  }, [gistState]);

  const handleValueChange = (gists, index, text) => {
    let updatedGist = [...gists];
    updatedGist[index] = text;
    setGists(updatedGist);
  };

  const handleAddFile = () => {
    const addFile = [...gists, ""];
    const addFilename = [...gistsFileName, ""];
    setGists(addFile);
    setGistsFileName(addFilename);
  };

  const handleDelFile = (index) => {
    let copyGist = [...gists];
    let copyGistFileName = [...gistsFileName];
    const gistArr1 = copyGist.splice(0, index);
    const gistArr2 = copyGist.splice(1, index + 1);
    const fileNameArr1 = copyGistFileName.splice(0, index);
    const fileNameArr2 = copyGistFileName.splice(1, index + 1);
    const delFile = [...gistArr1, ...gistArr2];
    const delFileName = [...fileNameArr1, ...fileNameArr2];
    setGists(delFile);
    setGistsFileName(delFileName);
  };

  const handleFileNameChange = ({ currentTarget: input }, index) => {
    const { value } = input;
    let fileNames = [...gistsFileName];
    fileNames[index] = value;
    setGistsFileName(fileNames);
  };

  const handleGistCreate = () => {
    const files = {};
    gistsFileName.map((fileName, index) => {
      files[fileName] = { content: gists[index], filename: fileName };
    });
    const reqData = createUserGists(files);
    async function CreateData() {
      await callGistCreateApi(reqData);
    }
    CreateData();
  };
  if (gistState.isLoading) {
    return (
      <LoaderWrapper>
        <AlignLoader>
          <Loader
            type="Oval"
            color="#0ca96e"
            height={100}
            width={100}
            visible={true}
          />
        </AlignLoader>
        <AlignLoader>
          <h3>Loading</h3>;
        </AlignLoader>
      </LoaderWrapper>
    );
  }

  if (!gistState.isLoading) {
    return (
      <>
        <MainWrapper>
          <ActionWrapper>
            <Button btnProps={buttonProps} onClick={handleGistCreate} />
          </ActionWrapper>
        </MainWrapper>
        {gists.map((gist, index) => (
          <>
            <CodeBlockWrapper key={index + "code"}>
              <CustomCodeBlock key={index + "codeblock"}>
                <FileName>
                  <InputField
                    name="file name"
                    value={gistsFileName[index]}
                    onChange={(e) => handleFileNameChange(e, index)}
                  />
                </FileName>
                <Editor
                  className="box"
                  value={gist}
                  onValueChange={(e) => {
                    handleValueChange(gists, index, e);
                  }}
                  highlight={(jsSample) => highlight(jsSample, languages.js)}
                />
              </CustomCodeBlock>
            </CodeBlockWrapper>
            <MainWrapper key={index + "key"}>
              <ActionWrapper>
                {index + 1 === gists.length && (
                  <Button
                    btnProps={addFileButtonProps}
                    onClick={handleAddFile}
                  />
                )}
                {index + 1 !== gists.length && (
                  <Button
                    btnProps={delFileButtonProps}
                    onClick={() => handleDelFile(index)}
                  />
                )}
              </ActionWrapper>
            </MainWrapper>
          </>
        ))}
      </>
    );
  }
};

export default CreateGistCodeEditor;
