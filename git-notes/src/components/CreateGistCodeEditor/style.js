import styled from "styled-components";

export const CodeBlockWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 6fr 1fr;
  grid-template-rows: 1fr;
`;

export const CustomCodeBlock = styled.div`
  grid-column: 2/3;
  border: 1px solid #efefef; //1px solid #b3aaaac7;
  margin-top: 3%;
  overflow-x: auto;
  padding: 30px;
  box-shadow: 0 10px 15px 5px rgba(0, 0, 0, 0.1),
    0 4px 25px -2px rgba(0, 0, 0, 0.05);
  border-radius: 15px;
`;

export const FileName = styled.div`
  border-bottom: 1px solid #b8bfb5d1;
  padding: 5px 0px;
  font-size: 12px;
  color: #4a81d4;
  font-weight: 700;
`;

export const MainWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 4fr 1fr;
  grid-template-rows: 1fr;
`;

export const LoaderWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
  // margin-left: 20px;
`;

export const AlignLoader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 20px;
`;

export const ProfileWrapper = styled.div`
  grid-column: 2/3;
  display: flex;
  flex-direction: row;
  align-items: center;
  color: #4a81d4;
`;

export const ActionWrapper = styled.div`
  height: 50px;
  pading-top: 10px;
  grid-column: 3/4;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;
