import React from "react";

const Edit = () => {
  return <i className="fa fa-pencil-square-o" aria-hidden="true"></i>;
};

export default Edit;
