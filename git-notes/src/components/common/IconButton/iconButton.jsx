import React from "react";
import { Button, CustomIcon } from "./style";
const IconButton = ({ btnProps, onPageChange, currentPage }) => {
  const getIcon = (iconClass) => {
    return (
      <CustomIcon
        className={iconClass}
        iconPosition={btnProps.iconPosition}
        aria-hidden="true"
      ></CustomIcon>
    );
  };
  if (btnProps.iconPosition === "left") {
    return (
      <Button
        backgroudColor={btnProps.backgroundColor}
        color={btnProps.color}
        onClick={onPageChange}
        disable={currentPage === 1}
      >
        {getIcon(btnProps.iconClass)}
        {btnProps.text}
      </Button>
    );
  }

  return (
    <Button
      backgroudColor={btnProps.backgroundColor}
      color={btnProps.color}
      onClick={onPageChange}
    >
      {btnProps.text}
      {getIcon(btnProps.iconClass)}
    </Button>
  );
};

export default IconButton;
