import styled from "styled-components";

export const Button = styled.button`
  width: 100%;
  height: 35px;
  max-width: 100px;
  color: ${(props) => props.color || "white"};
  border: none;
  border-radius: 5px;
  background-color: ${(props) => props.backgroudColor || "#0ca96e"};
  cursor: pointer;
`;

export const CustomIcon = styled.i`
  margin-left: ${(props) => (props.iconPosition === "left" ? "0px" : "10px")};
  margin-right: ${(props) => (props.iconPosition === "left" ? "10px" : "0px")};
`;
