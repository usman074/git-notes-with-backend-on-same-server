import React from "react";
import { CustomButton } from "./style";
const Button = ({ btnProps, icon = null, onClick }) => {
  return (
    <CustomButton
      backgroundColor={btnProps.backgroundColor}
      color={btnProps.color}
      height={btnProps.height ? btnProps.height : "100%"}
      width={btnProps.width ? btnProps.width : "100%"}
      marginLeft={btnProps.marginLeft ? btnProps.marginLeft : "10%"}
      marginRight={btnProps.marginRight ? btnProps.marginRight : "10%"}
      border={btnProps.border ? btnProps.border : "none"}
      borderRadius={btnProps.borderRadius ? btnProps.borderRadius : "none"}
      onClick={onClick}
    >
      {btnProps.text}
      <span>{icon}</span>
    </CustomButton>
  );
};

export default Button;
