import React from "react";
import { AvatarImg } from "./style";
const Avatar = ({ imgUrl, onClick = null }) => {
  if (onClick) {
    return (
      <AvatarImg src={imgUrl} onClick={() => onClick()} alt="avatar-img" />
    );
  }
  return <AvatarImg src={imgUrl} alt="avatar-img" />;
};

export default Avatar;
