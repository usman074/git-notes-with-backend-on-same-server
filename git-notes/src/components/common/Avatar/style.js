import styled from "styled-components";

export const AvatarImg = styled.img`
  width: 100%;
  max-width: 50px;
  margin-right: 10px;
  border-radius: 50%;
  height: 50px;
`;
