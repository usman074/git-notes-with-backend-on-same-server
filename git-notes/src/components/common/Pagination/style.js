import styled from "styled-components";

export const PaginationWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: 1fr;
  padding-top: 20px;
`;

export const PrevButtonWrapper = styled.div`
  grid-column: 1/2;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

export const NextButtonWrapper = styled.div`
  grid-column: 3/4;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;
