import React from "react";
import IconButton from "../IconButton";
import {
  PaginationWrapper,
  PrevButtonWrapper,
  NextButtonWrapper,
} from "./style";
const Pagination = ({ onPageIncrement, onPageDecrement, currentPage }) => {
  const prevButtonProps = {
    text: "Prev Page",
    backgroundColor: "#0ca96e",
    color: "white",
    iconClass: "fa fa-arrow-left",
    iconPosition: "left",
  };

  const nextButtonProps = {
    text: "Next Page",
    backgroundColor: "#0ca96e",
    color: "white",
    iconClass: "fa fa-arrow-right",
    iconPosition: "right",
  };

  return (
    <PaginationWrapper>
      <PrevButtonWrapper>
        <IconButton btnProps={prevButtonProps} onPageChange={onPageDecrement} />
      </PrevButtonWrapper>

      <NextButtonWrapper>
        <IconButton
          btnProps={nextButtonProps}
          onPageChange={onPageIncrement}
          currentPage={currentPage}
        />
      </NextButtonWrapper>
    </PaginationWrapper>
  );
};

export default Pagination;
