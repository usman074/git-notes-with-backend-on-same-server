import styled from "styled-components";

export const CustomCodeBlock = styled.div`
  // grid-column: 2/3;
  width: ${(props) => props.width};
  height: 250px;
  border: 1px solid #efefef; //1px solid #b3aaaac7;
  margin-top: 3%;
  overflow-x: auto;
  padding: 30px;
  box-shadow: 0 10px 15px 5px rgba(0, 0, 0, 0.1),
    0 4px 25px -2px rgba(0, 0, 0, 0.05);
  border-radius: 15px;
`;

export const FileName = styled.div`
  // font-size: 12px;
  // color: #4a81d4;
  // font-weight: 700;
  border-bottom: 1px solid #b8bfb5d1;
  padding: 3px 0px;
  display: flex;
  flex-direction: row;
  align-items: center;
  color: #4a81d4;
`;
