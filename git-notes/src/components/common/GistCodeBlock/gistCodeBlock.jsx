import React, { useEffect, useState } from "react";
import { CodeBlock, a11yLight } from "react-code-blocks";
import { CustomCodeBlock, FileName } from "./style";
import Avtar from "../../common/Avatar";

const GistCodeBlock = ({ gists, width = null, height = null }) => {
  const [gistFiles, setGistFiles] = useState([]);
  useEffect(() => {
    const fileKeys = Object.keys(gists.files);
    setGistFiles(fileKeys);
  }, [gists]);

  return (
    <>
      {gistFiles.map((file, index) => (
        <CustomCodeBlock
          key={gists.id + file + index}
          width={width ? width : "100%"}
          heigth={height ? height : "100%"}
        >
          <FileName>
            <Avtar imgUrl={gists.owner.avatar_url} />
            <h4>
              {gists.owner.login}/{file}
            </h4>
          </FileName>

          <CodeBlock
            language=""
            text={gists.files[file].content}
            showLineNumbers={true}
            theme={a11yLight}
            wrapLines={true}
            codeBlock
          />
        </CustomCodeBlock>
      ))}
    </>
  );
};

export default GistCodeBlock;
