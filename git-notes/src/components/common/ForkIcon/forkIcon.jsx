import React, { useEffect } from "react";
import { forkGists } from "../../../actions/gistsAction";
import useApiCall from "../../../customHooks/apiCallHook";
import { CursorWrapper } from "./style";
import { toast } from "react-toastify";

const Fork = ({ gistId }) => {
  const [gistForkState, callApi] = useApiCall({
    response: null,
  });

  useEffect(() => {
    if (gistForkState.error) {
      toast.error(gistForkState.error);
    } else if (gistForkState.response) {
      toast.success("Gist forked");
    }
  }, [gistForkState]);
  const handleForkClick = (id) => {
    const reqData = forkGists(id);
    async function ForkGist() {
      const reqState = {
        response: null,
        error: "",
      };
      await callApi(reqData, reqState);
    }
    ForkGist();
  };
  return (
    <CursorWrapper
      className="fa fa-code-fork"
      aria-hidden="true"
      onClick={() => {
        handleForkClick(gistId);
      }}
    ></CursorWrapper>
  );
};

export default Fork;
