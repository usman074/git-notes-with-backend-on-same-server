import styled from "styled-components";

export const CursorWrapper = styled.i`
  cursor: pointer;
`;
