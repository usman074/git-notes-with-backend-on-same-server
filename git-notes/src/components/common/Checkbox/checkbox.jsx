import React from "react";
import {
  CheckboxContainer,
  HiddenCheckbox,
  StyledCheckbox,
  Icon,
} from "./style";
const Checkbox = ({ onClick, checked, ...props }) => {
  return (
    <CheckboxContainer>
      <HiddenCheckbox onChange={() => {}} checked={checked} {...props} />
      <StyledCheckbox checked={checked} onClick={onClick}>
        <Icon viewBox="0 0 24 24">
          <polyline points="20 6 9 17 4 12" />
        </Icon>
      </StyledCheckbox>
    </CheckboxContainer>
  );
};

export default Checkbox;
