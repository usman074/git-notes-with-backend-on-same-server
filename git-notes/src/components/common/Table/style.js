import styled from "styled-components";
import { Link } from "react-router-dom";

export const CustomTable = styled.table`
  // grid-column: 2/3;
  // overflow-x: auto;
  border-collapse: collapse;
  // table-layout: fixed;
  border-bottom: 1px solid #ddd;
  text-align: center;
  width: 100%;
`;

export const TableHeading = styled.th`
  border-bottom: 1px solid #ddd;
  text-align: center;
  padding: 15px;
  background-color: #def5ec;
  color: #4e4d4de6;
  // width: 14%;
`;

export const TableData = styled.td`
  border-bottom: 1px solid #ddd;
  text-align: center;
  padding: 15px;
  color: #4e4d4de6;
  width: ${(props) => props.widthSize.width || "14%"};
  // word-wrap: break-word;
  // word-break: break-all;
  // border: 1px solid red;
`;

export const AvatarWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-beetween;
`;

export const RouteLink = styled(Link)`
  color: #4e4d4de6;
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
`;

// export const TableText = styled.p`
//   overflow: hidden;
//   text-overflow: ellipsis;
//   border: 1px solid #000000;
// `;
