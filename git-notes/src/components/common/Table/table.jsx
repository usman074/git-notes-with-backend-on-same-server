import React from "react";
import Star from "../StarIcon";
import Fork from "../ForkIcon";
import Avtar from "../Avatar";
import {
  CustomTable,
  TableHeading,
  TableData,
  AvatarWrapper,
  RouteLink,
} from "./style";
const Table = ({ tableHeading, tableData }) => {
  const getCellValue = (item, column) => {
    const pathProps = column.path.split(".");

    let value = item;
    pathProps.map((path) => (value = value[path] || null));
    if (column.path === "star") {
      return <Star />;
    } else if (column.path === "fork") {
      return <Fork gistId={item.id} />;
    }
    if (column.path === "owner.login") {
      const imgUrl = item.owner.avatar_url;
      return (
        <AvatarWrapper>
          <Avtar imgUrl={imgUrl} />
          <RouteLink to={`/gist/${item.id}`}>
            <p>{value}</p>
          </RouteLink>
        </AvatarWrapper>
      );
    } else if (column.label === "Date") {
      return value.split("T")[0];
    } else if (column.label === "Time") {
      return value.split("T")[1].substring(0, 5);
    } else if (column.label === "Keyword") {
      let fileKeywords = [];
      const fileKeys = Object.keys(value);
      fileKeys.map((file) => {
        fileKeywords = [...fileKeywords, value[file].language];
      });
      return fileKeywords.join(", ");
    } else if (column.label === "Notebook Name") {
      const fileKeys = Object.keys(value);
      return fileKeys.join(", ");
    } else {
      return value;
    }
  };
  return (
    <CustomTable>
      <thead>
        <tr>
          {tableHeading.map((heading) => (
            <TableHeading key={heading.path + heading.label}>
              {heading.content ? heading.content : heading.label}
            </TableHeading>
          ))}
        </tr>
      </thead>
      <tbody>
        {tableData.map((item, index) => (
          <tr key={index}>
            {tableHeading.map((column) => (
              <TableData
                widthSize={column}
                key={item.id + column.path + column.label + index}
              >
                {getCellValue(item, column)}
              </TableData>
            ))}
          </tr>
        ))}
      </tbody>
    </CustomTable>
  );
};

export default Table;
