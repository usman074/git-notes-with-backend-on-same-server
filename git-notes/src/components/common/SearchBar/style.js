import styled from "styled-components";

export const CustomInput = styled.input`
  width: 100%;
  height: 30px;
  max-width: 250px;
  border: 1px solid #93e0c3;
  border-radius: 5px;
  position: relative;
  background-color: transparent;
  &::-webkit-input-placeholder {
    color: white;
  }
`;
export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;
export const CustomIcon = styled.i`
    padding 5px 15px 5px 15px;
    cursor: pointer;
    position: absolute;
  `;
