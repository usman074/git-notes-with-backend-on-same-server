import React from "react";
import { Wrapper, CustomIcon, CustomInput } from "./style";

const SearchBar = ({ name, value, onChange }) => {
  return (
    <Wrapper>
      <CustomInput
        type="text"
        name={name}
        key={name}
        placeholder="Search Notes..."
        value={value}
        onChange={(e) => onChange(e)}
      />
      <CustomIcon className="fa fa-search" aria-hidden="true"></CustomIcon>
    </Wrapper>
  );
};

export default SearchBar;
