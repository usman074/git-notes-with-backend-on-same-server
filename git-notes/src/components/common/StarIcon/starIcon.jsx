import React from "react";

const Star = () => {
  return <i className="fa fa-star-o" aria-hidden="true"></i>;
};

export default Star;
