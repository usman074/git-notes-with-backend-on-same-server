import React from "react";
import { CustomInput } from "./style";

const InputField = ({ name, value, onChange }) => {
  return (
    <CustomInput
      type="text"
      name={name}
      key={name}
      placeholder="File Name..."
      value={value}
      onChange={(e) => onChange(e)}
    />
  );
};

export default InputField;
