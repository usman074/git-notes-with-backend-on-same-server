import React from "react";

const Del = () => {
  return <i className="fa fa-trash-o" aria-hidden="true"></i>;
};

export default Del;
