import React from "react";
import { Route, Redirect } from "react-router-dom";
import { access_user_key } from "../../../authConfig.json";
import { useUserContext } from "../../../services/userCotextService";

const ProtectedRoute = ({ component: Component, render, ...rest }) => {
  let { state: contextUser } = useUserContext();
  if (contextUser.user === null) {
    contextUser = JSON.parse(localStorage.getItem(access_user_key));
  }
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!contextUser)
          return (
            <Redirect
              to={{
                pathname: "/gists/public",
                state: { from: props.location },
              }}
            />
          );
        return Component ? <Component {...props} /> : render(props);
      }}
    ></Route>
  );
};

export default ProtectedRoute;
