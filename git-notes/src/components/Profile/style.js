import styled from "styled-components";

export const MainWrapper = styled.div`
  display: grid;
  grid-template-columns: 10% 40% 40% 10%;
  grid-template-rows: 1fr;
  margin-top: 20px;
`;

export const ProfileWrapper = styled.div`
  grid-column: 2/3;
  color: #4e4d4de6;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const GistsWrapper = styled.div`
  grid-column: 3/4;
`;

export const ProfileImg = styled.img`
  width: 60%;
  // max-width: 50px;
  margin-right: 10px;
  border-radius: 50%;
  // height: 50px;
`;

export const LoaderWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
  // margin-left: 20px;
`;

export const AlignLoader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 20px;
`;
