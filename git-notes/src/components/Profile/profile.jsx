import React, { useState, useEffect } from "react";
import useApiCall from "../../customHooks/apiCallHook";
import { fetchUserGists } from "../../actions/gistsAction";
import Button from "../common/Button";
import Loader from "react-loader-spinner";
import { toast } from "react-toastify";
import GistCodeBlock from "../common/GistCodeBlock";
import {
  MainWrapper,
  ProfileWrapper,
  GistsWrapper,
  ProfileImg,
  LoaderWrapper,
  AlignLoader,
} from "./style";
import { useUserContext } from "../../services/userCotextService";
const Profile = () => {
  const { state: contextUser } = useUserContext();
  const [userGistsState, callApi] = useApiCall({
    response: [],
  });
  const [currentPage, setCurrentPage] = useState(1);
  const [userGists, setUserGists] = useState([]);

  const buttonProps = {
    text: "View GitHub Profile",
    backgroundColor: "white",
    color: "#4a81d4",
    height: "40px",
    width: "40%",
    marginLeft: "0%",
    marginRight: "0%",
    borderRadius: "5px",
    border: "1px solid #928c8c94",
  };

  useEffect(() => {
    const reqData = fetchUserGists(currentPage);
    async function fetchData() {
      const reqState = {
        response: [],
        error: "",
      };
      await callApi(reqData, reqState);
    }
    fetchData();
  }, [currentPage]);

  useEffect(() => {
    if (userGistsState.error) {
      toast.error(userGistsState.error);
      if (currentPage > 1) {
        setCurrentPage(currentPage - 1);
      }
    } else if (userGistsState.response) {
      setUserGists(userGistsState.response);
    }
  }, [userGistsState]);

  const handleProfileClick = () => {
    console.log("profile clicked");
  };
  if (userGistsState.isLoading || !userGists || !contextUser.user) {
    return (
      <LoaderWrapper>
        <AlignLoader>
          <Loader
            type="Oval"
            color="#0ca96e"
            height={100}
            width={100}
            visible={true}
          />
        </AlignLoader>
        <AlignLoader>
          <h3>Loading</h3>
        </AlignLoader>
      </LoaderWrapper>
    );
  }
  if (!userGistsState.isLoading && userGists && contextUser.user) {
    return (
      <MainWrapper>
        <ProfileWrapper>
          <ProfileImg src={contextUser.user.avatar_url} alt="avatar-img" />
          <h4>{contextUser.user.name}</h4>
          <Button btnProps={buttonProps} onClick={handleProfileClick} />
        </ProfileWrapper>
        <GistsWrapper>
          {userGists.map((gist) => (
            <GistCodeBlock gists={gist} />
          ))}
        </GistsWrapper>
      </MainWrapper>
    );
  }
};

export default Profile;
