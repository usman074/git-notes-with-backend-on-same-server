import React, { useEffect } from "react";
import { useUserContext } from "../../services/userCotextService";
import { loginUser } from "../../actions/authAction";
import useApiCall from "../../customHooks/apiCallHook";
import { access_user_key } from "../../authConfig.json";

const SettingUserContext = (props) => {
  const { state: contextUser, dispatch: setContextUser } = useUserContext();
  const [user, callApi] = useApiCall({ response: null, isLogin: false });
  useEffect(() => {
    if (
      props.accessToken !== undefined &&
      props.accessToken !== "undefined" &&
      props.accessToken !== null &&
      contextUser.user === null &&
      contextUser.isLogin === false
    ) {
      const reqData = loginUser(props.accessToken);
      async function fetchData() {
        const reqState = {
          response: null,
          error: "",
          isLogin: false,
        };
        await callApi(reqData, reqState, { isLogin: true });
      }
      fetchData();
    }
  }, [props.accessToken]);

  useEffect(() => {
    if (user.response) {
      localStorage.setItem(access_user_key, JSON.stringify(user));
      const requestAction = {
        type: "SAVE_USER_CONTEXT",
        payload: user.response,
      };
      setContextUser(requestAction);
    }
  }, [user]);

  return null;
};

export default SettingUserContext;
