import React, { useEffect } from "react";
import { useUserContext } from "../../services/userCotextService";
const Signout = (props) => {
  const { dispatch: setUserContext } = useUserContext();
  useEffect(() => {
    localStorage.clear();
    const requestAction = {
      type: "DEL_USER_CONTEXT",
      payload: null,
    };
    setUserContext(requestAction);
    props.history.push("/gists/public");
  });
  return null;
};

export default Signout;
