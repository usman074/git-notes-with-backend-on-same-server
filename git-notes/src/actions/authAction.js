import { client_id, client_secret } from "../authConfig.json";
export const getUserAccessToken = (code) => {
  return {
    url: `/api/user/accessToken?client_id=${client_id}&client_secret=${client_secret}&code=${code}`,
    options: {
      method: "get",
    },
    payload: {},
    meta: { type: "api" },
  };
};

export const loginUser = (accessToken) => {
  return {
    url: `/api/user/login?accessToken=${accessToken}`,
    options: {
      method: "get",
    },
    payload: {},
    meta: { type: "api" },
  };
};
