import { access_token_key } from "../authConfig.json";
export const fetchPublicGists = (currentPage) => {
  const token = localStorage.getItem(access_token_key);
  return {
    url: `/api/gists/public?currentPage=${currentPage}`,
    options: {
      method: "get",
      headers: { Authorization: "token " + token },
    },
    payload: {},
    meta: { type: "api" },
  };
};

export const fetchUserGists = (currentPage) => {
  const token = localStorage.getItem(access_token_key);

  return {
    url: `/api/gists?currentPage=${currentPage}`,
    options: {
      method: "get",
      headers: { Authorization: "token " + token },
    },
    payload: {},
    meta: { type: "api" },
  };
};

export const fetchStarredGists = (currentPage) => {
  const token = localStorage.getItem(access_token_key);

  return {
    url: `/api/gists/starred?currentPage=${currentPage}`,
    options: {
      method: "get",
      headers: { Authorization: "token " + token },
    },
    payload: {},
    meta: { type: "api" },
  };
};

export const fetchSingleGist = (gistId) => {
  const token = localStorage.getItem(access_token_key);
  return {
    url: `/api/gists/${gistId}`,
    options: {
      method: "get",
      headers: { Authorization: "token " + token },
    },
    payload: {},
    meta: { type: "api" },
  };
};

export const updateUserGists = (gist) => {
  if (gist.description === null) {
    gist.description = "";
  }
  const token = localStorage.getItem(access_token_key);
  return {
    url: `/api/gists`,
    options: {
      method: "put",
      headers: { Authorization: "token " + token },
    },
    payload: { gist },
    meta: { type: "api" },
  };
};

export const createUserGists = (gist) => {
  const token = localStorage.getItem(access_token_key);
  return {
    url: `/api/gists`,
    options: {
      method: "post",
      headers: { Authorization: "token " + token },
    },
    payload: { files: gist, description: "" },
    meta: { type: "api" },
  };
};

export const forkGists = (id) => {
  const token = localStorage.getItem(access_token_key);
  return {
    url: `/api/gists/fork`,
    options: {
      method: "post",
      headers: { Authorization: "token " + token },
    },
    payload: { id },
    meta: { type: "api" },
  };
};

export const delUserGist = (gistId) => {
  const token = localStorage.getItem(access_token_key);
  return {
    url: `/api/gists/${gistId}`,
    options: {
      method: "delete",
      headers: { Authorization: "token " + token },
    },
    payload: {},
    meta: { type: "api" },
  };
};
