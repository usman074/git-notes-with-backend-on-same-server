const axios = require("axios");
const { base_url } = require("../config.json");

const sendReq = async (url, options, payload) => {
  try {
    const response = await axios({
      method: options.method,
      headers: options.headers ? options.headers : null,
      url: base_url + url,
      data: payload,
    });
    return response;
  } catch (error) {
    return error;
  }
};

const loginUser = async (url, options, payload) => {
  try {
    const response = await axios({
      method: options.method,
      headers: options.headers ? options.headers : null,
      url: url,
      data: payload,
    });
    return response;
  } catch (error) {
    return error;
  }
};

module.exports = { sendReq, loginUser };
